package com.daffodil.cms.controller;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daffodil.cms.entity.CmsChannel;
import com.daffodil.cms.entity.CmsDocument;
import com.daffodil.cms.service.ICmsChannelService;
import com.daffodil.cms.service.ICmsDocumentService;
import com.daffodil.core.annotation.FormToken;
import com.daffodil.core.annotation.FormToken.TokenType;
import com.daffodil.core.annotation.Log;
import com.daffodil.core.annotation.Log.BusinessType;
import com.daffodil.core.controller.BaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.Page;
import com.daffodil.core.entity.TableInfo;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.util.text.Convert;

/**
 * 
 * @author yweijian
 * @date 2020年10月25日
 * @version 1.0
 * @description
 */
@Controller
@RequestMapping("/cms/document")
public class CmsDocumentController extends BaseController {
	
	private String prefix = "cms/document";

	@Autowired
	private ICmsDocumentService documentService;
	
	@Autowired
	private ICmsChannelService channelService;

	@RequiresPermissions("cms:document:view")
	@GetMapping()
	public String document() {
		return prefix + "/document";
	}

	/**
	 * 查询文章列表
	 */
	@SuppressWarnings("unchecked")
	@RequiresPermissions("cms:document:list")
	@GetMapping("/list")
	@ResponseBody
	public TableInfo list(CmsDocument document) {
		initQuery(document, new Page());
		List<CmsDocument> list = documentService.selectDocumentList(query);
		return initTableInfo(list, query);
	}

	/**
	 * 新增文章
	 */
	@FormToken
	@GetMapping("/add/{channelId}")
	public String add(@PathVariable("channelId") String channelId, ModelMap modelMap) {
		modelMap.put("user", ShiroUtils.getSysUser());
		CmsChannel channel = channelService.selectChannelById(channelId);
		modelMap.put("channel", channel);
		return prefix + "/add";
	}

	/**
	 * 新增保存文章
	 */
	@FormToken(TokenType.DESTROY)
	@RequiresPermissions("cms:document:add")
	@Log(title = "稿件管理", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public JsonResult addSave(CmsDocument document) {
		documentService.insertDocument(document);
		return JsonResult.success();
	}

	/**
	 * 修改文章
	 */
	@GetMapping("/edit/{documentId}")
	public String edit(@PathVariable("documentId") String documentId, ModelMap modelMap) {
		CmsDocument document = documentService.selectDocumentById(documentId);
		modelMap.put("document", document);
		modelMap.put("user", ShiroUtils.getSysUser());
		CmsChannel channel = channelService.selectChannelById(document.getChannelId());
		modelMap.put("channel", channel);
		return prefix + "/edit";
	}

	/**
	 * 修改保存文章
	 */
	@RequiresPermissions("cms:document:edit")
	@Log(title = "稿件管理", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(CmsDocument document) {
		documentService.updateDocument(document);
		return JsonResult.success();
	}

	/**
	 * 删除文章
	 */
	@RequiresPermissions("cms:document:remove")
	@Log(title = "稿件管理", businessType = BusinessType.DELETE)
	@PostMapping("/remove")
	@ResponseBody
	public JsonResult remove(String ids) {
		documentService.deleteDocumentByIds(Convert.toStrArray(ids));
		return JsonResult.success();
	}
}
