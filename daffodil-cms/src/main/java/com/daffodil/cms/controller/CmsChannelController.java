package com.daffodil.cms.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.daffodil.cms.entity.CmsChannel;
import com.daffodil.cms.service.ICmsChannelService;
import com.daffodil.core.annotation.FormToken;
import com.daffodil.core.annotation.FormToken.TokenType;
import com.daffodil.core.annotation.Log;
import com.daffodil.core.annotation.Log.BusinessType;
import com.daffodil.core.controller.BaseController;
import com.daffodil.core.entity.JsonResult;
import com.daffodil.core.entity.TableInfo;
import com.daffodil.core.entity.Ztree;

/**
 * 栏目（站点）控制层
 * @author yweijian
 * @date 2020年10月23日
 * @version 1.0
 * @description
 */
@Controller
@RequestMapping("/cms/channel")
public class CmsChannelController extends BaseController {
	private String prefix = "cms/channel";

	@Autowired
	private ICmsChannelService channelService;

	@SuppressWarnings("unchecked")
	@RequiresPermissions("cms:channel:list")
	@GetMapping("/list")
	@ResponseBody
	public TableInfo list(CmsChannel channel) {
		initQuery(channel);
		List<CmsChannel> channelList = channelService.selectChannelList(query);
		return initTableInfo(channelList);
	}

	/**
	 * 新增栏目
	 */
	@FormToken
	@GetMapping("/add/{parentId}")
	public String add(@PathVariable("parentId") String parentId, ModelMap modelMap) {
		modelMap.put("channel", channelService.selectChannelById(parentId));
		return prefix + "/add";
	}

	/**
	 * 新增保存栏目
	 */
	@FormToken(TokenType.DESTROY)
	@Log(title = "栏目管理", businessType = BusinessType.INSERT)
	@RequiresPermissions("cms:channel:add")
	@PostMapping("/add")
	@ResponseBody
	public JsonResult addSave(@Validated CmsChannel channel) {
		channelService.insertChannel(channel);
		return JsonResult.success();
	}

	/**
	 * 修改
	 */
	@GetMapping("/edit/{channelId}")
	public String edit(@PathVariable("channelId") String channelId, ModelMap modelMap) {
		CmsChannel channel = channelService.selectChannelById(channelId);
		modelMap.put("channel", channel);
		CmsChannel parent = channelService.selectChannelById(channel.getParentId());
		modelMap.put("parent", parent);
		return prefix + "/edit";
	}

	/**
	 * 保存
	 */
	@Log(title = "栏目管理", businessType = BusinessType.UPDATE)
	@RequiresPermissions("cms:channel:edit")
	@PostMapping("/edit")
	@ResponseBody
	public JsonResult editSave(@Validated CmsChannel channel) {
		channelService.updateChannel(channel);
		return JsonResult.success();
	}

	/**
	 * 删除
	 */
	@Log(title = "栏目管理", businessType = BusinessType.DELETE)
	@RequiresPermissions("cms:channel:remove")
	@PostMapping("/remove/{channelId}")
	@ResponseBody
	public JsonResult remove(@PathVariable("channelId") String channelId) {
		channelService.deleteChannelById(channelId);
		return JsonResult.success();
	}

	/**
	 * 校验栏目名称
	 */
	@GetMapping("/checkChannelNameUnique")
	@ResponseBody
	public String checkChannelNameUnique(CmsChannel channel) {
		return channelService.checkChannelNameUnique(channel) ? "1" : "0";
	}

	/**
	 * 选择栏目树
	 */
	@GetMapping("/selectChannelTree/{channelId}")
	public String selectChannelTree(@PathVariable("channelId") String channelId, ModelMap modelMap) {
		modelMap.put("channel", channelService.selectChannelById(channelId));
		return prefix + "/tree";
	}

	/**
	 * 加载栏目列表树
	 */
	@SuppressWarnings("unchecked")
	@GetMapping("/treeData")
	@ResponseBody
	public List<Ztree> treeData(CmsChannel channel) {
		initQuery(channel);
		List<Ztree> ztrees = channelService.channelTreeData(query);
		return ztrees;
	}
	
	/**
	 * 批量新增栏目
	 */
	@FormToken
	@GetMapping("/batch/{parentId}")
	public String batch(@PathVariable("parentId") String parentId, ModelMap modelMap) {
		modelMap.put("channel", channelService.selectChannelById(parentId));
		return prefix + "/batch";
	}

	/**
	 * 批量保存栏目
	 */
	@FormToken(TokenType.DESTROY)
	@Log(title = "栏目管理", businessType = BusinessType.INSERT)
	@RequiresPermissions("cms:channel:add")
	@PostMapping("/batch")
	@ResponseBody
	public JsonResult batchSave(String parentId, String channels) {
		channelService.batchAddChannel(parentId, channels);
		return JsonResult.success();
	}
}
