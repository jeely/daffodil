package com.daffodil.cms.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daffodil.cms.entity.CmsChannelDocument;
import com.daffodil.cms.entity.CmsDocument;
import com.daffodil.cms.entity.CmsDocumentHis;
import com.daffodil.cms.service.ICmsDocumentService;
import com.daffodil.core.dao.JpaDao;
import com.daffodil.core.entity.Query;
import com.daffodil.core.exception.BaseException;
import com.daffodil.framework.shiro.util.ShiroUtils;
import com.daffodil.util.HqlUtils;
import com.daffodil.util.StringUtils;

/**
 * 
 * @author yweijian
 * @date 2020年10月25日
 * @version 1.0
 * @description
 */
@Service
public class CmsDocumentServiceImpl implements ICmsDocumentService {
	
	@Autowired
	private JpaDao jpaDao;
	
	@Override
	public List<CmsDocument> selectDocumentList(Query<CmsDocument> query) {
		StringBuffer hql = new StringBuffer("from CmsDocument where 1=1 ");
		List<Object> paras = new ArrayList<Object>();
		HqlUtils.createHql(hql, paras, query);
		return jpaDao.search(hql.toString(), paras, CmsDocument.class, query.getPage());
	}
	
	@Override
	public CmsDocument selectDocumentById(String documentId) {
		return jpaDao.find(CmsDocument.class, documentId);
	}

	@Override
	@Transactional
	public void insertDocument(CmsDocument document) {
		document.setCreateBy(ShiroUtils.getLoginName());
		document.setCreateTime(new Date());
		//去除html标签
		String content = document.getHtmlContent().replaceAll("<[^>]+>", "");
		document.setContent(content);
		document.setWordsCount(content.length());
		jpaDao.save(document);
		
		//栏目--文章关系表
		CmsChannelDocument channelDocument = new CmsChannelDocument();
		channelDocument.setSiteId(document.getSiteId());
		channelDocument.setChannelId(document.getChannelId());
		channelDocument.setDocumentId(document.getId());
		channelDocument.setCreateBy(ShiroUtils.getLoginName());
		channelDocument.setCreateTime(new Date());
		jpaDao.save(channelDocument);
		
		//历史文章记录表
		BeanCopier beanCopier = BeanCopier.create(CmsDocument.class, CmsDocumentHis.class, false);
		CmsDocumentHis documentHis = new CmsDocumentHis();
		beanCopier.copy(document, documentHis, null);
		documentHis.setId(null);
		documentHis.setDocumentId(document.getId());
		jpaDao.save(documentHis);
	}

	@Override
	@Transactional
	public void updateDocument(CmsDocument document) {
		//获取栏目--文章关系表
		List<Object> paras = new ArrayList<Object>();
		paras.add(document.getSiteId());
		paras.add(document.getChannelId());
		paras.add(document.getId());
		String hql = "from CmsChannelDocument where siteId = ? and channelId = ? and documentId = ?";
		CmsChannelDocument channelDocument = jpaDao.find(hql, paras , CmsChannelDocument.class);
		if(StringUtils.isNull(channelDocument)) {//关系表为空,非法操作
			throw new BaseException("文章【" + document.getTitle() +"】修改失败");
		}
		document.setUpdateBy(ShiroUtils.getLoginName());
		document.setUpdateTime(new Date());
		//去除html标签
		String content = document.getHtmlContent().replaceAll("<[^>]+>", "");
		document.setContent(content);
		document.setWordsCount(content.length());
		jpaDao.update(document);
		
		//历史文章记录表
		BeanCopier beanCopier = BeanCopier.create(CmsDocument.class, CmsDocumentHis.class, false);
		CmsDocumentHis documentHis = new CmsDocumentHis();
		beanCopier.copy(document, documentHis, null);
		documentHis.setId(null);
		documentHis.setDocumentId(document.getId());
		jpaDao.save(documentHis);
	}

	@Override
	@Transactional
	public void deleteDocumentByIds(String[] ids) {
		jpaDao.delete(CmsDocument.class, ids);
		List<Object> paras = new ArrayList<Object>();
		String hql = "delete from CmsChannelDocument where documentId in " + HqlUtils.createHql(paras , ids);
		jpaDao.delete(hql, paras);
	}
}
