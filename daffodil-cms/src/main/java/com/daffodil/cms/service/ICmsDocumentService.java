package com.daffodil.cms.service;

import java.util.List;

import com.daffodil.cms.entity.CmsDocument;
import com.daffodil.core.entity.Query;

/**
 * 文章稿件
 * @author yweijian
 * @date 2020年10月25日
 * @version 1.0
 * @description
 */
public interface ICmsDocumentService {

	/**
	 * 查询文章列表
	 * @param query
	 * @return
	 */
	public List<CmsDocument> selectDocumentList(Query<CmsDocument> query);
	
	/**
	 * 查询文章信息
	 * @param documentId
	 * @return
	 */
	public CmsDocument selectDocumentById(String documentId);

	/**
	 * 新增文章
	 * @param document
	 */
	public void insertDocument(CmsDocument document);

	/**
	 * 修改文章
	 * @param document
	 */
	public void updateDocument(CmsDocument document);

	/**
	 * 删除文章信息
	 * @param ids
	 */
	public void deleteDocumentByIds(String[] ids);
}
