package com.daffodil.core.entity;

import java.io.Serializable;

/**
 * 国密SM2
 * @author yweijian
 * @date 2020年12月16日
 * @version 1.0
 * @description
 */
public class SM2Key implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String priKey;      //私钥
	
	private String pubKey;      //公钥

	public  SM2Key(String priKey,String pubKey){
		this.priKey = priKey;
		this.pubKey = pubKey;
	}

	public String getPriKey() {
		return priKey;
	}
	public void setPriKey(String priKey) {
		this.priKey = priKey;
	}
	public String getPubKey() {
		return pubKey;
	}
	public void setPubKey(String pubKey) {
		this.pubKey = pubKey;
	}
}
