package com.daffodil.jasypt.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.daffodil.jasypt.util.JasyptUtils;

/**
 * 
 * @author yweijian
 * @date 2021年2月7日
 * @version 1.0
 * @description
 */
public class JasyptUI extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private static transient Logger logger = LoggerFactory.getLogger(JasyptUI.class);

	private JPanel mainPanel;
	
	private JLabel lblPasswordContent;

	private JTextArea textPasswordContent;

	private JLabel lblPlainContent;

	private JTextArea textPlainContent;

	private JLabel lblJasypContent;

	private JTextArea textJasypContent;

	private JLabel errorInfo;

	private JButton jasypEncyptBtn;

	private JButton jasypDecyptBtn;

	private JButton clearBtn;
	
	private static final String BLANK = "";
	
	private static final String PASSWORD = "daffodil";
	
	public void initJasypUI() {
		try {
			setDefaultCloseOperation(3);
			this.mainPanel = new JPanel();
			getContentPane().add(this.mainPanel, "Center");
			this.mainPanel.setPreferredSize(new Dimension(600, 480));
			this.mainPanel.setLayout((LayoutManager) null);
			
			this.lblPasswordContent = new JLabel();
			this.lblPasswordContent.setText("密盐：");
			this.lblPasswordContent.setBounds(30, 10, 45, 20);
			this.mainPanel.add(this.lblPasswordContent);
			
			this.textPasswordContent = new JTextArea(3, 50);
			this.textPasswordContent.setText(PASSWORD);
			this.textPasswordContent.setBounds(70, 10, 380, 20);
			this.textPasswordContent.setPreferredSize(new Dimension(90, 25));
			this.mainPanel.add(this.textPasswordContent);
			
			this.lblPlainContent = new JLabel();
			this.lblPlainContent.setText("明文：");
			this.lblPlainContent.setBounds(30, 40, 45, 20);
			this.mainPanel.add(this.lblPlainContent);
			
			this.textPlainContent = new JTextArea(3, 50);
			this.textPlainContent.setLineWrap(true);
			this.textPlainContent.setBounds(70, 40, 380, 60);
			this.textPlainContent.setPreferredSize(new Dimension(90, 25));
			this.mainPanel.add(this.textPlainContent);
			
			this.lblJasypContent = new JLabel();
			this.lblJasypContent.setText("密文：");
			this.lblJasypContent.setBounds(30, 110, 45, 20);
			this.mainPanel.add(this.lblJasypContent);
			
			this.textJasypContent = new JTextArea(3, 50);
			this.textJasypContent.setLineWrap(true);
			this.textJasypContent.setBounds(70, 110, 380, 60);
			this.textJasypContent.setPreferredSize(new Dimension(90, 25));
			this.mainPanel.add(this.textJasypContent);
			
			this.errorInfo = new JLabel();
			this.errorInfo.setForeground(Color.RED);
			this.errorInfo.setBounds(150, 180, 220, 15);
			this.errorInfo.setBackground(Color.green);
			this.errorInfo.setHorizontalAlignment(0);
			this.errorInfo.setPreferredSize(new Dimension(109, 21));
			this.errorInfo.setLocale(new Locale("zh", "CN"));
			this.mainPanel.add(this.errorInfo);
			
			this.jasypEncyptBtn = new JButton();
			this.jasypEncyptBtn.setBounds(110, 205, 100, 25);
			this.mainPanel.add(this.jasypEncyptBtn);
			this.jasypEncyptBtn.setText("加 密");
			this.jasypEncyptBtn.addActionListener(this);
			
			this.jasypDecyptBtn = new JButton();
			this.jasypDecyptBtn.setBounds(210, 205, 100, 25);
			this.mainPanel.add(this.jasypDecyptBtn);
			this.jasypDecyptBtn.setText("解 密");
			this.jasypDecyptBtn.addActionListener(this);
			
			this.clearBtn = new JButton();
			this.clearBtn.setBounds(310, 205, 100, 25);
			this.mainPanel.add(this.clearBtn);
			this.clearBtn.setText("清 空");
			this.clearBtn.addActionListener(this);
			
			setTitle("JASYP加解密工具");
			setSize(520, 280);
			setVisible(true);
			setAlwaysOnTop(true);
			setResizable(false);
			setDefaultCloseOperation(3);
			setLocationRelativeTo(null);
			
		} catch (Exception e) {
			logger.error("初始化界面[" + JasyptUI.class.getName() + "]出错", e);
		}
	}

	public void actionPerformed(ActionEvent aEvent) {
		JButton button = (JButton) aEvent.getSource();
		if (button.getText().equals("加 密")) {
			String passwordContent = this.textPasswordContent.getText();
			if (StringUtils.isBlank(passwordContent)) {
				this.errorInfo.setText("请输入加密的密盐");
				this.textPasswordContent.requestFocus();
				return;
			}
			String plainContent = this.textPlainContent.getText();
			if (StringUtils.isBlank(plainContent)) {
				this.errorInfo.setText("请输入待加密的文本");
				this.textPlainContent.requestFocus();
				return;
			}
			this.errorInfo.setText(BLANK);
			String jasypContent = JasyptUtils.encyptPwd(passwordContent, plainContent);
			this.textJasypContent.setText(jasypContent);
		} else if (button.getText().equals("解 密")) {
			String passwordContent = this.textPasswordContent.getText();
			if (StringUtils.isBlank(passwordContent)) {
				this.errorInfo.setText("请输入加密的密盐");
				this.textPasswordContent.requestFocus();
				return;
			}
			String jasypContent = this.textJasypContent.getText();
			if (StringUtils.isBlank(jasypContent)) {
				this.errorInfo.setText("请输入待解密的文本");
				this.textJasypContent.requestFocus();
				return;
			}
			this.errorInfo.setText(BLANK);
			String plainContent = JasyptUtils.decyptPwd(passwordContent, jasypContent);
			this.textPlainContent.setText(plainContent);
		} else if (button.getText().equals("清 空")) {
			this.textPlainContent.setText(BLANK);
			this.textJasypContent.setText(BLANK);
			this.errorInfo.setText(BLANK);
			this.textPlainContent.requestFocus();
		}
	}
}
