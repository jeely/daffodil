package com.daffodil.easyfile.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 文件配置
 * @author yweijian
 * @date 2021年1月6日
 * @version 1.0
 * @description
 */
@Configuration
@ConfigurationProperties(prefix = "daffodil.easyfile.config")
public class EasyfileConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 是否使用文件后缀扩展名校验
	 */
	private Boolean suffix;
	
	/**
	 * 是否使用文件媒体类型校验
	 */
	private Boolean mime;
	
	/**
	 * [业务]允许文件后缀扩展名
	 */
    private Map<String, String> suffixs = new HashMap<String, String>();
    
    /**
	 * [业务]允许文件媒体类型
	 */
    private Map<String, String> mimes = new HashMap<String, String>();

	public Boolean getSuffix() {
		return suffix;
	}

	public void setSuffix(Boolean suffix) {
		this.suffix = suffix;
	}

	public Boolean getMime() {
		return mime;
	}

	public void setMime(Boolean mime) {
		this.mime = mime;
	}

	public Map<String, String> getSuffixs() {
		return suffixs;
	}

	public void setSuffixs(Map<String, String> suffixs) {
		this.suffixs = suffixs;
	}

	public Map<String, String> getMimes() {
		return mimes;
	}

	public void setMimes(Map<String, String> mimes) {
		this.mimes = mimes;
	}
	
	/**
	 * 根据业务获取允许上传得配置
	 * @param business 业务名称
	 * @return
	 */
	public Map<String, Object> getEasyfileConfigByBusiness(String business){
		Map<String, Object> allowConfig = new HashMap<String, Object>();
		allowConfig.put("suffix", this.getSuffix());
		allowConfig.put("suffixs", this.getSuffixs().get(business));
		allowConfig.put("mime", this.getMime());
		allowConfig.put("mimes", this.getMimes().get(business));
		return allowConfig;
	}

}
