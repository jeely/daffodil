package com.daffodil.system.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Excel;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统访问记录表
 * 
 * @author yweijian
 * @date 2019年8月16日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_login_info")
public class SysLoginInfo extends BaseEntity {
	private static final long serialVersionUID = 4756609331723695083L;

	/** 登录信息编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "login_info_id")
	@Excel(name = "登录信息编号")
	private String id;
	
	/** 用户账号 */
	@Column(name = "login_name")
	@Excel(name = "用户账号")
	@Hql(type = Logical.LIKE)
	private String loginName;

	/** 登录状态 0成功 1失败 */
	@Column(name = "status")
	@Dict(value = "sys_success_status")
	@Excel(name = "登录状态", readConverterExp = "0=成功,1=失败")
	@Hql(type = Logical.EQ)
	private String status;

	/** 登录IP地址 */
	@Column(name = "ipaddr")
	@Excel(name = "登录地址")
	@Hql(type = Logical.LIKE)
	private String ipaddr;

	/** 登录地点 */
	@Column(name = "login_location")
	@Excel(name = "登录地点")
	@Hql(type = Logical.LIKE)
	private String loginLocation;

	/** 浏览器类型 */
	@Column(name = "browser")
	@Excel(name = "浏览器")
	@Hql(type = Logical.LIKE)
	private String browser;

	/** 操作系统 */
	@Column(name = "os")
	@Excel(name = "操作系统 ")
	@Hql(type = Logical.LIKE)
	private String os;

	/** 提示消息 */
	@Column(name = "msg")
	@Excel(name = "提示消息")
	@Hql(type = Logical.LIKE)
	private String msg;

	/** 访问时间 */
	@Column(name = "create_time")
	@Excel(name = "访问时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	
}