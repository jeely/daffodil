package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.daffodil.core.entity.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户和岗位关联
 * 
 * @author yweijian
 * @date 2019年8月15日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sys_user_post")
public class SysUserPost extends BaseEntity {
	private static final long serialVersionUID = -255423089769176273L;

	/** 角用户和岗位关联编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "user_post_id")
	private String id;
	
	/** 用户ID */
	@Column(name = "user_id")
	private String userId;

	/** 岗位ID */
	@Column(name = "post_id")
	private String postId;

}
