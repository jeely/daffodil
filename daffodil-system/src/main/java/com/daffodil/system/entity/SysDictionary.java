package com.daffodil.system.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;

import java.util.Date;

import com.daffodil.core.annotation.Dict;
import com.daffodil.core.annotation.Hql;
import com.daffodil.core.annotation.Hql.Logical;
import com.daffodil.core.entity.Ztree;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典表
 * @author weiji
 * @date 2020年4月30日
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name="sys_dictionary")
public class SysDictionary extends Ztree {
	private static final long serialVersionUID = 3256222817665807324L;

	/** 字典编号 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@Column(name = "dict_id")
	private String id;
	
	/** 字典（目录）名称 */
	@NotBlank(message = "字典名称不能为空")
	@Size(min = 0, max = 50, message = "字典名称长度不能超过50个字符")
	@Hql(type = Logical.LIKE)
	@Column(name = "dict_name")
	private String dictName;
	
	/** 字典类型（catalog目录 dictionary字典） */
	@Dict("sys_dict_type")
	@Hql(type = Logical.EQ)
	@Column(name = "dict_type")
	private String dictType;
	
	/** 目录键值 */
	@Size(min = 0, max = 50, message = "字典键值长度不能超过50个字符")
	@Hql(type = Logical.EQ)
	@Column(name = "dict_label")
	private String dictLabel;
	
	/** 字典键值 */
	@Hql(type = Logical.EQ)
	@Column(name = "dict_value")
	private String dictValue;
	
	/** 是否默认（Y是 N否） */
	@Dict("sys_yes_no")
	@Hql(type = Logical.EQ)
	@Column(name = "is_default")
	private String isDefault;
	
	/** 父字典ID */
	@Hql(type = Logical.EQ)
	@Column(name = "parent_id")
	private String parentId;
	
	/** 祖级列表 */
	@Column(name = "ancestors")
	private String ancestors;

	/** 显示顺序 */
	@Column(name = "order_num")
	private Long orderNum;
	
	/** 字典状态 */
	@Dict("sys_data_status")
	@Hql(type = Logical.EQ)
	@Column(name = "status")
	private String status;
	
	/** 创建者 */
	@Column(name="create_by")
	@Hql(type = Logical.LIKE)
	private String createBy;

	/** 创建时间 */
	@Column(name="create_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/** 更新者 */
	@Column(name="update_by")
	@Hql(type = Logical.LIKE)
	private String updateBy;

	/** 更新时间 */
	@Column(name="update_time")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	/** 备注 */
	@Column(name="remark")
	@Hql(type = Logical.LIKE)
	private String remark;

}
